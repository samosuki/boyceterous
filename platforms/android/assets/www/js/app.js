var boyceterous = angular.module('boycterous', ['ngRoute', 'ui.bootstrap', 'boycterous.directives'])

.config(function($routeProvider, $locationProvider) {
  $routeProvider.when('/', {
    templateUrl: 'templates/tours.html',
    controller: 'HomeCtrl'
  });

  $routeProvider.when('/tours/:tourid', {
    templateUrl: 'templates/tourdetail.html',
    controller: 'TourCtrl'
  });

  $routeProvider.when('/tours/order/:tourid', {
    templateUrl: 'templates/tourorder.html',
    controller: 'OrderCtrl'
  });

  $routeProvider.when('/specials', {
    templateUrl: 'templates/specials.html',
    controller: 'SpecialsCtrl'
  });

  $routeProvider.otherwise({
    redirectTo: '/'
  });
})

/*** Controllers ***/

.controller('HomeCtrl', ['$scope',
	function($scope) {

}])
.controller('TourCtrl', ['$scope',
	function($scope) {

}])
.controller('SpecialsCtrl', ['$scope',
  function($scope) {

}])
.controller('OrderCtrl', ['$scope', '$http', '$routeParams',
  function($scope, $http, $routeParams) {

  $scope.cruises = 
    [
      {
      tourid: 1,
      title: "Snorkeling Adventure Cruise",
      description: "Swimming with the turtles and visiting the shipwreck at the beautiful Carlisle Bay. Finger foods, drinks, snorkeling gear and music will be provided. Come have your photos taken with the turtles and have a blast", 
      adultprice: 45,
      childprice: "22.50",
      img: ""
    },
    {
      tourid: 2,
      title: "West Coast Tropical Morning Cruise",
      description: "View the shipwreck at Carlisle Bay, swim with the turtles. Finger foods, drinks, snorkeling gear, buffet lunch and music will be provided. Come have a blast as we party our way along the West Coast", 
      adultprice: 85,
      childprice: 40,
      img: ""
    },
    {
      tourid: 3,
      title: "West Coast Tropical Evening Cruise",
      description: "This evening cruise from 2 pm - 6:30 pm includes viewing the shipwreck at Carlisle Bay and swimming with the turtles. Finger foods, drinks, snorkeling gear, buffet lunch and music will be provided. Come have a blast as we party our way along the West Coast.", 
      adultprice: 85,
      childprice: 40,
      img: ""
    }
   ];

   $scope.tours = _.findWhere($scope.cruises, { tourid: +$routeParams.tourid });

  $scope.haserror = false;
  $scope.msgsent = false;

  $scope.sendbtntext = "Send Cruise Request";

  $scope.open = function($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.opened = true;
  };

  $scope.dateOptions = {
    'year-format': "'yy'",
    'starting-day': 1
  };

  $scope.onSubmitCruise = function() {
      //console.log('button clicked');
      $scope.sendbtntext = "Sending Request...";      
      $http.post('http://cruising.reddonor.com/api/send-cruise/' + $routeParams.tourid, $scope.pass).
        success(function(data) {
          //console.log(data);
          if(data.haserror) {
            $scope.haserror = true;
          }
          if(data.msgsent) {
            $scope.msgsent = true;
            //console.log("msg sent true");
          }
      });
    };
  
}]);

