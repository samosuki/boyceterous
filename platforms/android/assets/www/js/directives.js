/*******************************************
 Directives 
*******************************************/
var boyceterous = angular.module('boycterous.directives', []);

boyceterous.directive('tourslist', ['$http', '$routeParams', function($http, $routeParams) {
	var tourslistFn;
	tourslistFn = function(scope, element, attrs) {

		scope.tours = 
		[
			{ 
			tourid: 1, 
			title: "Snorkeling Adventure Cruise",
			description: "Swimming with the turtles and visiting the shipwreck at the beautiful Carlisle Bay. Finger foods, drinks, snorkeling gear and music will be provided. Come have your photos taken with the turtles and have a blast", 
			adultprice: 45,
			childprice: "22.50",
			img:"img/turtle.jpg" 
		},
		{ 
			tourid: 2,
			title: "West Coast Tropical Morning Cruise",
			description: "View the shipwreck at Carlisle Bay, swim with the turtles. Finger foods, drinks, snorkeling gear, buffet lunch and music will be provided. Come have a blast as we party our way along the West Coast",
			adultprice: 85,
			childprice: 40,
			img: "img/morning.jpg"
		},
		{ 
			tourid: 3, 
			title: "West Coast Tropical Evening Cruise", 
			description: "This evening cruise from 2 pm - 6:30 pm includes viewing the shipwreck at Carlisle Bay and swimming with the turtles. Finger foods, drinks, snorkeling gear, buffet lunch and music will be provided. Come have a blast as we party our way along the West Coast.", 
			adultprice: 85, 
			childprice: 40, 
			img: "img/evening.jpg" 
		}
	 ]
		
	};
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'templates/tourslist.html',
		link: tourslistFn
	};
}])

.directive('toursdetail', ['$http', '$routeParams', function($http, $routeParams) {
	var tourslistdetailFn;
	tourslistdetailFn = function(scope, element, attrs) {

		scope.cruises = 
		[
			{ 
			tourid: 1, 
			title: "Snorkeling Adventure Cruise",
			description: "Swimming with the turtles and visiting the shipwreck at the beautiful Carlisle Bay. Finger foods, drinks, snorkeling gear and music will be provided. Come have your photos taken with the turtles and have a blast.", 
			adultprice: 45,
			childprice: "22.50",
			img:"img/turtle.jpg"
		},
		{ 
			tourid: 2, 
			title: "West Coast Tropical Morning Cruise", 
			description: "View the shipwreck at Carlisle Bay, swim with the turtles. Finger foods, drinks, snorkeling gear, buffet lunch and music will be provided. Come have a blast as we party our way along the West Coast.", 
			adultprice: 85, 
			childprice: 40, 
			img: "img/morning.jpg" 
		},
		{ 
			tourid: 3, 
			title: "West Coast Tropical Evening Cruise", 
			description: "This evening cruise from 2 pm - 6:30 pm includes viewing the shipwreck at Carlisle Bay and swimming with the turtles. Finger foods, drinks, snorkeling gear, buffet lunch and music will be provided. Come have a blast as we party our way along the West Coast.", 
			adultprice: 85, 
			childprice: 40, 
			img: "img/evening.jpg"  
		}
	 ]

	 scope.tours = _.findWhere(scope.cruises, { tourid: +$routeParams.tourid });
		//tourid: +$routeParams.tourid 
	 //});

		/*console.log(scope.cruises);
		console.log($routeParams.tourid);
		console.log(scope.tours);*/
		/*$http.get('http://cruising.reddonor.com/api/tours/' + $routeParams.tourid).
				success(function(data) {
					scope.tours = data.tours;
					console.log(scope.tours);
					return;
			});*/
	};
	return {
		restrict: 'E',
		replace: true,
		templateUrl: 'templates/tourdetaillist.html',
		link: tourslistdetailFn
	};
}])
	.directive('phone', function() {
		return {
				restrice: 'A',
				require: 'ngModel',
				link: function(scope, element, attrs, ctrl) {
						var PHONE_REGEXP = /^(\d+-?)+\d+$/;
						angular.element(element).bind('blur', function() {
								var value = this.value;
								if(PHONE_REGEXP.test(value)) {
										// Valid input
										console.log("valid phone number");
										//angular.element(this).next().next().css('display','none');  
								} else {
										// Invalid input  
										console.log("invalid phone number");
										angular.element(this).next().next().css('ng-ditry');
										/* 
												Looks like at this point ctrl is not available,
												so I can't user the following method to display the error node:
												ctrl.$setValidity('currencyField', false); 
										*/                    
								}
						});              
				}            
		}        
});



